package com.wisniew.cherrybashcommander

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.tab6_fragment.*
import android.R.attr.process
import android.util.Log
import java.io.IOException
import java.io.OutputStreamWriter


class Tab6Fragment : Fragment() {

    private var checkbutton: Button? = null

    fun execCommand(command: String) {
        val runtime = Runtime.getRuntime()
        var proc: Process? = null
        var osw: OutputStreamWriter? = null

        try {
            proc = runtime.exec("su")
            osw = OutputStreamWriter(proc!!.outputStream)
            osw!!.write(command)
            osw!!.flush()
            osw!!.close()
        } catch (ex: IOException) {
            Log.e("execCommand()", "Command resulted in an IO Exception: $command")
            return
        } finally {
            if (osw != null) {
                try {
                    osw!!.close()
                } catch (e: IOException) {
                }

            }
        }

        try {
            proc!!.waitFor()
        } catch (e: InterruptedException) {
        }

        if (proc!!.exitValue() != 0) {
            Log.e("execCommand()", "Command returned error: " + command + "\n  Exit code: " + proc.exitValue())
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.tab6_fragment, container, false)

        // Check button
        checkbutton = view.findViewById(R.id.checkbutton)
        checkbutton!!.setOnClickListener {

            execCommand("sh /sdcard/Android/data/com.wisniew.cherrybashcommander/cbc-script.sh 2")
        }

        return view
    }

    companion object {
        private val TAG = "Tab6Fragment"
    }
}
