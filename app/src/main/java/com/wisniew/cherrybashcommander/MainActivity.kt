package com.wisniew.cherrybashcommander

import android.support.design.widget.TabLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.wisniew.cherrybashcommander.R.id.toolbar

import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.io.OutputStreamWriter

class MainActivity : AppCompatActivity() {

    private var mSectionsPageAdapter: SectionsPageAdapter? = null

    private var mViewPager: ViewPager? = null

    var version = BuildConfig.VERSION_NAME

    fun execCommand(command: String) {
        val runtime = Runtime.getRuntime()
        var proc: Process? = null
        var osw: OutputStreamWriter? = null

        try {
            proc = runtime.exec("su")
            osw = OutputStreamWriter(proc!!.outputStream)
            osw!!.write(command)
            osw!!.flush()
            osw!!.close()
        } catch (ex: IOException) {
            Log.e("execCommand()", "Command resulted in an IO Exception: $command")
            return
        } finally {
            if (osw != null) {
                try {
                    osw!!.close()
                } catch (e: IOException) {
                }

            }
        }

        try {
            proc!!.waitFor()
        } catch (e: InterruptedException) {
        }

        if (proc!!.exitValue() != 0) {
            Log.e("execCommand()", "Command returned error: " + command + "\n  Exit code: " + proc.exitValue())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        mSectionsPageAdapter = SectionsPageAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById<ViewPager>(R.id.container)
        setupViewPager(mViewPager!!)

        val tabLayout = findViewById<TabLayout>(R.id.tabs)
        tabLayout.setupWithViewPager(mViewPager)

        execCommand("mkdir -p /sdcard/Android/data/com.wisniew.cherrybashcommander")
        execCommand("curl -L -o /sdcard/Android/data/com.wisniew.cherrybashcommander/cbc-script.sh \"https://drive.google.com/uc?export=download&id=1t1VdntQPCU2Mq9bEZeensFNifGXHm659\"")

        execCommand("sh /sdcard/Android/data/com.wisniew.cherrybashcommander/cbc-script.sh 1 0.1.1")
        execCommand("touch /sdcard/Android/data/com.wisniew.cherrybashcommander/test.txt ")
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = SectionsPageAdapter(supportFragmentManager)
        adapter.addFragment(Tab1Fragment(), "Info") //TODO Add strings from trings.xml
        adapter.addFragment(Tab2Fragment(), "CPU") //TODO Add strings from trings.xml
        adapter.addFragment(Tab3Fragment(), "GPU") //TODO Add strings from trings.xml
        adapter.addFragment(Tab4Fragment(), "I/O") //TODO Add strings from trings.xml
        adapter.addFragment(Tab5Fragment(), "RAM") //TODO Add strings from trings.xml
        adapter.addFragment(Tab6Fragment(), "Updates") //TODO Add strings from trings.xml
        viewPager.adapter = adapter
    }

    companion object {

        private val TAG = "MainActivity"
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Adds menu main
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Menu items selected
        val id = item.itemId

        if (id == R.id.menu_settings) {
            return true
        }

        if (id == R.id.menu_flasher) {
            return true
        }

        if (id == R.id.menu_about) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }



}
